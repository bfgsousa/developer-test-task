<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTrigger extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared('CREATE TRIGGER `TInsertTransaction` AFTER INSERT ON `transactions` FOR EACH ROW
        BEGIN
            IF(new.deposit) THEN
                UPDATE customers SET balance = balance + new.amount WHERE id = new.customer_id;
            ELSE
                SET @balance := (SELECT balance FROM customers WHERE id = new.customer_id);
                IF(@balance >= new.amount) THEN
                        UPDATE customers SET balance = balance - new.amount WHERE id = new.customer_id;
                ELSE
                        SIGNAL SQLSTATE "45000"
                        SET MESSAGE_TEXT = "Cannot withdraw more than your balance";
                    END IF;
            END IF;
        END');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('DROP TRIGGER `TInsertTransaction`');
    }
}
