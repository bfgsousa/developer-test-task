<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    public $timestamps = false;
    protected $fillable = ['amount','deposit'];
    public function customer() 
    {
        return $this->belongsTo('App\Customer');
    }
}
