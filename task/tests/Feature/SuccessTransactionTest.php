<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Customer;
use App\Transaction;
use Illuminate\Database\QueryException;

class SuccessTransactionTest extends TestCase
{
    //use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testExample()
    {
        $error = false;
        $customer = factory(Customer::class)->create();
        try {
            $customer->transactions()->saveMany([
                new Transaction(['amount' => 20.00, 'deposit' => 1]),
                new Transaction(['amount' => 20.00, 'deposit' => 0]),
            ]);
        }catch(QueryException $ex){ 
            if(strpos($ex->getMessage(),'Cannot withdraw more than your balance') !== false)
                $error = true;
        }finally {
            $this->assertFalse($error);
        }
        
    }
}
