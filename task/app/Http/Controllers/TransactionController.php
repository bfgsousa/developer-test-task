<?php

namespace App\Http\Controllers;

use App\Transaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Customer;

class TransactionController extends Controller
{

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'amount' => 'required|numeric',
            'deposit' => 'required|boolean',
            'customer_id' => 'required|exists:App\Customer,id'
        ]);

        $customer = Customer::find($request->customer_id);

        if ($validator->fails()) {    
            return response()->json($validator->messages(), 400);
        }

        $error = false;
        $transaction = null;
        
        try {
            $transaction = $customer->transactions()->save(new Transaction(['amount' => $request->amount, 'deposit' => $request->deposit]));
        }catch(QueryException $ex) { 
            //ignore
        }finally {
            if($transaction) {
                return response()->json($transaction->customer->balance, 201);
            }else {
                return response()->json('Cannot withdraw more than your balance', 400);
            }
        }
    }

}
