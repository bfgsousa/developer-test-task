# Developer Test Task

Developer Test Task is test requested by KCS IT to test my full stack skills and to be discuted on interview.

## Tecnologies:

  - Laravel 6
  - Vue.js
  - Bootstrap
  - SCSS

## Requirements

  - MySQL Database
  - Apache/NGINX Server
  - PHP >= 9.2
  - Composer
  - NPM

## Installation

```sh
$ git clone git@gitlab.com:bfgsousa/developer-test-task.git
$ cd developer-test-task/task
$ cp .env.example .env
$ vi .env //Change Database Variables
$ composer install
$ php artisan migrate
$ npm install
$ php artisan key:generate
$ npm run prod
$ php artisan serve
```

See the [result](http://localhost:8000)